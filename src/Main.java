import com.example.BankA;
import com.example.BankB;
import com.example.BankC;

public class Main {
    public static void main(String[] args) {
        BankA bankA=new BankA();
        System.out.println(bankA.getBalance());
        BankB bankB=new BankB();
        System.out.println(bankB.getBalance());
        BankC bankC=new BankC();
        System.out.println(bankC.getBalance());
    }
}
